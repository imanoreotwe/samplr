#include <cmath>
#include <QColor>

#include "waveform.h"
#include "window.cc"

Waveform::Waveform(QWidget *parent) : QOpenGLWidget(parent) {
    background = QBrush(QColor(161, 173, 186)); // Grey?

    index = 0;
    //setFixedSize(parent->width(), parent->height());
    setAutoFillBackground(true); // @TODO change this?
}

Waveform::~Waveform() { }

void Waveform::setWave(std::shared_ptr<Wave> &w) {
    wave = w;
    buildPeak(0, wave->sampleRate()*20, this->width());
}

void Waveform::paintEvent(QPaintEvent *event) {
    if (wave != nullptr) {
        QPainter painter;
        painter.begin(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.fillRect(event->rect(), background);
        //painter.translate(100,100);
        //painter.setPen(QPen(QColor(52, 152, 219), 1, Qt::SolidLine, Qt::RoundCap)); // blue?
        painter.setPen(QPen(QColor(225, 225, 225), 1, Qt::SolidLine, Qt::RoundCap)); // blue?

        int min_x = event->region().boundingRect().x();
        int max_x = min_x + event->region().boundingRect().width();

        int min_y = this->height();
        int mid_y = min_y/2;
        int right_y = mid_y - min_y/4;
        int left_y = mid_y + min_y/4;

        auto scale = [min_y](int a) {
            int x0 = 1 - pow(2,15);
            int y0 = pow(2,15) - 1;
            int x1 = 0;
            int y1 = min_y/2;

            return x1 + (y1 - x1) * ((a - x0)/(y0-x0));
        };
        //painter.drawLine(max_x/2, mid_y, max_x/2, mid_y + 14);
        int chop_len = max_x / NUMPADS;
        auto scalefactor = 0.000015;
        //auto scalefactor = 0.0000015;
        for(auto i = min_x; i < max_x; i++) {
            if(i >= peaks.size()) break;
            if(i % chop_len == 0) {
                painter.save();
                painter.setPen(QPen(QColor(218, 72, 72), 1, Qt::SolidLine, Qt::RoundCap)); // blue?
                painter.drawLine(i, 0, i, min_y);
                painter.restore();
            } else {
                //auto wth = scale(data[index]);
                auto wtf = ((min_y/4)*peaks[i].first*(scalefactor));
                painter.drawLine(i, right_y, i, right_y + wtf);
                painter.drawLine(i, right_y, i, right_y - wtf);
                //painter.drawLine(i, left_y, i+6, 2*i);

                //wtf = ((min_y/4)*data[index+1]*(0.00000001));
                wtf = ((min_y/4)*peaks[i+1].first*(scalefactor));
                painter.drawLine(i, left_y, i, left_y + wtf);
                painter.drawLine(i, left_y, i, left_y - wtf);
            }
        }
        painter.end();
    }
}

void Waveform::resizeEvent(QResizeEvent *event) { }

/*
 * Builds a smooth vector of samples from wave.
 *
 */
void Waveform::buildPeak(size_t start, size_t end, int size) {
    const uint16_t *data = reinterpret_cast<const uint16_t *>(wave->data());
    auto chunk = (end-start)/size;
    auto sum = 0;
    //auto peak = 0;
    int j = 0;
    for(size_t i = start; i < end; i++) {
        if(i % chunk == 0) {
            peaks.push_back(std::make_pair(sum/chunk, i));
            //peaks.push_back(std::make_pair(peak, i));
            sum = 0;
            //peak = 0;
            j++;
        } else {
            //if (data[i] > peak) peak = data[i];
            sum += data[i];
        }
    }

    if(peaks.size() < size*2) {
        for (size_t i = peaks.size() - (size*2-peaks.size()); i <= size*2; i++) {
            peaks.push_back(std::make_pair(0,i));
        }
    }
}
