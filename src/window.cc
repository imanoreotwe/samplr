#include <iostream>

#include <QtWidgets>
#include <QTimer>
#include <QFileDialog>

#include "window.h"
#include "audio.h"

Window::Window(QWidget *parent) : QMainWindow(parent), audio() {
    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    QWidget *topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QWidget *bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    waveform = new Waveform(this);
    waveform->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(5, 5, 5, 5);
    layout->addWidget(topFiller);
    layout->addWidget(waveform);
    layout->addWidget(bottomFiller);
    widget->setLayout(layout);

    createActions();
    createMenus();

    setFixedSize(2000, 900);
}


void Window::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_A: {
            audio.play();
            break;
        }
        case Qt::Key_S : {
            audio.stop();
            break;
        }
    }
}

/* --- Slots --- */

void Window::newFile() { }

void Window::openWaveFile() {
    QString fileName = QFileDialog::getOpenFileName(this,
    tr("Open Wave"), "", tr("Wave Files (*.wav *.wave)"));

    wave = std::shared_ptr<Wave>(new Wave(fileName.toStdString()));
    audio.openStream(wave);
    waveform->setWave(wave); // could be cleaner with slots
    waveform->repaint();
}

/* --- end slots --- */

void Window::createActions() {
    newProj = new QAction(tr("&New"), this);
    newProj->setShortcuts(QKeySequence::New);
    newProj->setStatusTip(tr("Create a new project"));
    connect(newProj, &QAction::triggered, this, &Window::newFile);

    openWave = new QAction(tr("&Open Wave"), this);
    openWave->setStatusTip(tr("Open a wave to sample"));
    connect(openWave, &QAction::triggered, this, &Window::openWaveFile);
}

void Window::createMenus() {
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newProj);
    //fileMenu->addAction(openProj);
    //fileMenu->addAction(saveProj);
    // I'd like a visual break here
    fileMenu->addAction(openWave);
}
