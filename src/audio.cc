#include <cstring>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <string>

#include "audio.h"

Audio::Audio() {
    PaError err = Pa_Initialize();
    if(err != paNoError) {
        throw std::string("Portaudio failed to initalize. ") + Pa_GetErrorText(err);
    }
    stream = nullptr;
}

Audio::~Audio() {
    if(stream != nullptr) {
        PaError err = Pa_CloseStream(stream);
        if( err != paNoError ) {
            std::cerr << "Portaudio failed to close stream.\n";
        }

        err = Pa_Terminate();
        if( err != paNoError ) {
            std::cerr << "Portaudio failed to terminate.\n";
        }
    }
}

int Audio::outputCallback( const void *inputBuffer,
						   void *outputBuffer,
					   	   unsigned long frameCount,
					   	   const PaStreamCallbackTimeInfo* timeInfo,
					   	   PaStreamCallbackFlags statusFlags,
					   	   void *userData )
{
	Wave *wave = reinterpret_cast<Wave *>(userData);
	uint32_t* src = (uint32_t*)wave->data();
	uint8_t *out = (uint8_t*)outputBuffer;
	size_t frameSize = wave->channels() * (wave->bitsPerSample()/8);
	size_t maxIndex = wave->subChunk2Size() / frameSize;

	size_t framesLeft = maxIndex - wave->index();
	long framesEmpty = frameCount - framesLeft;
	src += wave->index();

	if (framesLeft >= frameCount) {
		memcpy(out, src, frameCount * frameSize);
		out = out + frameCount * frameSize;
	} else {
		//put some
		memcpy(out, src, framesLeft * frameSize);
		//zero some
		if (framesEmpty > 0) {
			memset(out+framesLeft * frameSize, 0, framesEmpty * frameSize);
			out = out + frameCount * frameSize;
			wave->clearIndex();
			return paComplete;
		}
	}
	wave->incrementIndex(frameCount);

    (void) inputBuffer;
    (void) timeInfo;
    (void) statusFlags;

	return paContinue;
}

void Audio::openStream(std::shared_ptr<Wave> &w) {
    w->clearIndex();

	PaStreamParameters outputParams;
	outputParams.device = Pa_GetDefaultOutputDevice();
	if(outputParams.device == paNoDevice) {
        throw "No default output device.";
	}

	outputParams.channelCount = w->channels();
    switch(w->bitsPerSample()) {
        case 8: outputParams.sampleFormat = paInt8; break;
        case 16: outputParams.sampleFormat = paInt16; break;
        case 32: outputParams.sampleFormat = paInt32; break;
        default: throw "Could not set sample format.";
    }
	outputParams.suggestedLatency = Pa_GetDeviceInfo( outputParams.device)->defaultLowOutputLatency;
    outputParams.hostApiSpecificStreamInfo = NULL;

	PaError err = Pa_OpenStream(
        &stream,
		NULL,
		&outputParams,
		w->sampleRate(),
		paFramesPerBufferUnspecified,
		0,
		&Audio::outputCallback,
		w.get()
	);
	if( err != paNoError) {
        throw std::string("Portaudio failed to open stream. ") + Pa_GetErrorText(err);
    }
}

void Audio::play() {
    if(Pa_IsStreamActive(stream)) {
        stop();
        return;
    }
    PaError err = Pa_StartStream(stream);
	if( err != paNoError) {
        throw std::runtime_error("Portaudio failed to start stream.");
    }
}

void Audio::stop() {
    PaError err = Pa_StopStream(stream);
	if( err != paNoError) {
        throw std::runtime_error("Portaudio failed to stop stream.");
    }
}
