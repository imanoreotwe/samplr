#include <cstdint>
#include <cstdio>
#include <cstring>
#include <cerrno>

#include "wave.h"

Wave::Wave(const Wave &w)  :
    _wave{w._wave}, _samples(w._samples), _index(0) { }

Wave::Wave(std::string file) : _wave() {
    FILE *ptr;
    ptr = fopen(file.c_str(), "rb");
    if(ptr == NULL) {
        int errsv = errno;
        throw "Error opening " + file + ". " + strerror(errsv);
    }

    _wave.header = new struct HEADER;
    _index = 0;

    // Filling the header
    chop_fread(ptr, 4, _wave.header->chunkId);
    _wave.header->chunkSize = chop_fread<uint32_t>(ptr);
    chop_fread(ptr, 4, _wave.header->format);
    chop_fread(ptr, 4, _wave.header->subChunk1Id);
    _wave.header->subChunk1Size = chop_fread<uint32_t>(ptr);
    _wave.header->audioFormat = chop_fread<uint16_t>(ptr);
    _wave.header->channels = chop_fread<uint16_t>(ptr);
    _wave.header->sampleRate = chop_fread<uint32_t>(ptr);
    _wave.header->byteRate = chop_fread<uint32_t>(ptr);
    _wave.header->blockAlign = chop_fread<uint16_t>(ptr);
    _wave.header->bitsPerSample = chop_fread<uint16_t>(ptr);
    chop_fread(ptr, 4, _wave.header->subChunk2Id);
    _wave.header->subChunk2Size = chop_fread<uint32_t>(ptr);

    // fill the data chunk
    _samples = _wave.header->subChunk2Size/sizeof(char);
    _wave.data = new char[_samples];
    size_t read = fread(_wave.data, sizeof(char), _samples, ptr);
    if(read != _samples && ferror(ptr)) {
        throw "Error reading samples.";
    }
    fclose(ptr);
}

Wave::~Wave() {
    delete _wave.header; // @NOTE segfault risk with this
    delete[] _wave.data;
}

Wave &Wave::operator=(const Wave &w) {
    _wave = w._wave; //@TODO this is a nono
    _samples = w._samples;
    return *this;
}

struct WAVE* Wave::wave() {
    return &_wave;
}

const char* Wave::data() const{
    return _wave.data;
}

short int Wave::channels() {
    return _wave.header->channels;
}

short int Wave::bitsPerSample() {
    return _wave.header->bitsPerSample;
}

int Wave::sampleRate() {
    return _wave.header->sampleRate;
}

int Wave::subChunk2Size() {
    return _wave.header->subChunk2Size;
}

size_t Wave::samples() {
    return _samples;
}

size_t Wave::index() {
    return _index;
}

void Wave::clearIndex() {
    _index = 0;
}

void Wave::incrementIndex(size_t i) {
    _index += i;
}

template<typename T>
T Wave::chop_fread(FILE *stream) {
    T value;
    size_t read = fread(&value, sizeof(value), 1, stream);
    if(read != 1 && ferror(stream) ) {
        throw "fread error";
    }
    return value;
}

void Wave::chop_fread(FILE *stream, size_t len, char* fill) {
    size_t read = fread(fill, 1, len, stream);
    if (read != len && ferror(stream) ) {
        throw "fread error";
    }
    fill[len] = '\0';
}
