#include <iostream>
#include <exception>

#include <QApplication>

#include "window.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    Window mainWindow;
    try {
        mainWindow.show();
    } catch (const std::exception &e) {
        std::cerr << "ERROR: " << e.what() << '\n';
    }

    return app.exec();
}
