#include <unistd.h>
#include <cstdlib>
#include <cerrno>
#include <cstdint>
#include <cstring>

#include <portaudio.h>

#include <string>
#include <iostream>

#include "wave.h"
#include "audio.h"

using namespace std;

static string program_name;

void input_loop(Audio &a) {
    char c;
    PaError err;
    while(true){
        cin >> c;
        switch(c) {
            case 'p': {
            	// start stream
                a.play();
            	if( err != paNoError) {
                    return;
                }
                break;
            }
            case 's': {
                // stop stream
                a.stop();
            	if( err != paNoError) {
                    return;
                }
                break;
            }
            case 'q': {
                return;
            }
        }
    }
}

int main(int argc, char **argv) {
	program_name = argv[0];

    Wave wave(argv[1]);
    Audio audio;
    audio.openStream(wave);

    input_loop(audio);

    return 0;
}
