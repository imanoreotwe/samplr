# samplr

simple music sampler to learn about audio and graphics programming.
some goals:

+ cross platform: ideally I'd like to be able to run this on all the major OSs
+ midi controllers
+ audio effects

## running
pretty straight forward

```
make
./samplr
```

## deps
To build:
+ glfw
+ [glad](https://github.com/Dav1dde/glad) `pip install --user glad`
+ [Native File Dialog Extended](https://github.com/btzy/nativefiledialog-extended)
