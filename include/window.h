#ifndef WINDOW_SAMPLER_H
#define WINDOW_SAMPLER_H

#include <memory>

#include <QMainWindow>
#include <QKeyEvent>

#include "wave.h"
#include "audio.h"
#include "waveform.h"

#define NUMPADS 10

class Window : public QMainWindow {
    Q_OBJECT
  public:
    Window(QWidget *parent = 0);

  protected:
    void keyPressEvent(QKeyEvent *event) override;

  public slots:
    void newFile();
    //void open();
    //void save();
    void openWaveFile();

  private:
    QMenu *fileMenu;

    QAction *newProj;
    QAction *openProj;
    QAction *saveProj;
    QAction *openWave;

    Audio audio;
    std::shared_ptr<Wave> wave;
    Waveform *waveform;

    void createActions();
    void createMenus();

};

#endif
