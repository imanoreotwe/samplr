#ifndef AUDIO_SAMPLER_H
#define AUDIO_SAMPLER_H

#include <memory>
#include <portaudio.h>

#include "wave.h"

class Audio {
  public:
    Audio();
    ~Audio();

    static int outputCallback( const void *inputBuffer,
    						   void *outputBuffer,
    					   	   unsigned long frameCount,
    					   	   const PaStreamCallbackTimeInfo* timeInfo,
    					   	   PaStreamCallbackFlags statusFlags,
    					   	   void *userData );


    void openStream(std::shared_ptr<Wave> &wave);
    void play();
    void stop();

  private:
    PaStream *stream;
};

#endif
