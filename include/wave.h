#ifndef WAVE_SAMPLER_H
#define WAVE_SAMPLER_H

#include <string>

struct HEADER {
    char        chunkId[4];         // RIFF String
    int         chunkSize;          // overall size in bytes
    char        format[4];          // WAVE string

    char        subChunk1Id[4];     // fmt string
    int         subChunk1Size;      // length of format subchunk
    short int   audioFormat;        // format type. 1-PCM, 3- IEEE float, 6- 8bit A law, 7- 8bit mu law
    short int   channels;
    int         sampleRate;         // sampling rate (blocks per second)
    int         byteRate;           // SampleRate * Channels * BitsPerSample/8
    short int   blockAlign;         // Channels * BitsPerSample/8
    short int   bitsPerSample;      // bits per sample 8/16

    char        subChunk2Id[4];     // DATA string or FLLR string
    int         subChunk2Size;      // NumSamples * FrameSize
};

struct WAVE {
    struct HEADER *header;
    char* data;
};

class Wave {
  public:
    Wave() = delete;
    Wave(const Wave &w);
    Wave(const std::string file); // @TODO &file
    ~Wave();

    Wave &operator=(const Wave &w);

    struct WAVE* wave();
    const char* data() const;
    short int channels();
    short int bitsPerSample();
    int sampleRate();
    int subChunk2Size();
    size_t samples();
    size_t index();

    void clearIndex();
    void incrementIndex(size_t i);

  private:
    struct WAVE _wave;
    size_t _samples;
    size_t _index;
    
    template<typename T>
    T chop_fread(FILE *stream);
    void chop_fread(FILE *stream, size_t len, char* fill);


};

#endif
