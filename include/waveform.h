#ifndef WAVEFORM_SAMPLER_H
#define WAVEFORM_SAMPLER_H

#include <memory>
#include <vector>
#include <utility>
#include <cstdint>

#include <QBrush>
#include <QPainter>
#include <QOpenGLWidget>
#include <QPaintEvent>
#include <QWidget>

#include "wave.h"

class Waveform : public QOpenGLWidget {
    Q_OBJECT
  public:
    Waveform() = delete;
    Waveform(QWidget *parent);
    //Waveform(const Waveform &w);
    ~Waveform();

    //Waveform &operator=(const Waveform &w);

    void setWave(std::shared_ptr<Wave> &wave);

  protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

  private:
    QBrush background;

    std::shared_ptr<Wave> wave;
    size_t index;
    std::vector<std::pair<uint16_t, size_t>> peaks;

    void buildPeak(size_t start, size_t end, int size);
};

#endif
