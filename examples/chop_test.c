#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "portaudio.h"

// WAVE file header format
struct HEADER {
	char chunkId[4];						// RIFF string
	int chunkSize;				// overall size of file in bytes
	char format[4];						// WAVE string

	char subChunk1Id[4];			// fmt string with trailing null char
	int length_of_fmt;					// length of the format data
	short int format_type;					// format type. 1-PCM, 3- IEEE float, 6 - 8bit A law, 7 - 8bit mu law
	short int channels;						// no.of channels
	int sample_rate;					// sampling rate (blocks per second)
	int byterate;						// SampleRate * NumChannels * BitsPerSample/8
	short int block_align;					// NumChannels * BitsPerSample/8
	short int bits_per_sample;				// bits per sample, 8- 8bits, 16- 16 bits etc

	char subChunk2Id[4];		// DATA string or FLLR string
	int subChunk2Size;						// NumSamples * NumChannels * BitsPerSample/8 - size of the next chunk that will be read
};

struct WAV {
    struct HEADER *header;
    char* data;
    long long int index;
    long long int size;
    long long int numSamples;
};

/* Util functions for endien shit */

int isBigEndian() {
    int test = 1;
    char *p = (char*)&test;

    return p[0] == 0;
}
void reverseEndianness(const long long int size, void* value){
    int i;
    char result[32];
    for( i=0; i<size; i+=1 ){
        result[i] = ((char*)value)[size-i-1];
    }
    for( i=0; i<size; i+=1 ){
        ((char*)value)[i] = result[i];
    }
}
void toBigEndian(const long long int size, void* value){
    char needsFix = !( (1 && isBigEndian()) || (0 && !isBigEndian()) );
    if( needsFix ){
        reverseEndianness(size,value);
    }
}
void toLittleEndian(const long long int size, void* value){
    char needsFix = !( (0 && isBigEndian()) || (1 && !isBigEndian()) );
    if( needsFix ){
        reverseEndianness(size,value);
    }
}

/* --- end endian functions --- */

/* header functions */

int fillHeader(struct HEADER *header, FILE *ptr) {
    unsigned char buffer4[4];
    unsigned char buffer2[2];


    int read = 0;


    read = fread(header->chunkId, sizeof(header->chunkId), 1, ptr);

     // convert little endian to big endian 4 byte int
     read = fread(buffer4, sizeof(buffer4), 1, ptr);
     header->chunkSize = buffer4[0] |
    						(buffer4[1]<<8) |
    						(buffer4[2]<<16) |
    						(buffer4[3]<<24);

    read = fread(header->format, sizeof(header->format), 1, ptr);

    read = fread(header->subChunk1Id, sizeof(header->subChunk1Id), 1, ptr);

    // convert little endian to big endian 4 byte integer
    read = fread(buffer4, sizeof(buffer4), 1, ptr);
    header->length_of_fmt = buffer4[0] |
    							(buffer4[1] << 8) |
    							(buffer4[2] << 16) |
    							(buffer4[3] << 24);

    read = fread(buffer2, sizeof(buffer2), 1, ptr);
    header->format_type = buffer2[0] | (buffer2[1] << 8);


    read = fread(buffer2, sizeof(buffer2), 1, ptr);
    header->channels = buffer2[0] | (buffer2[1] << 8);

    read = fread(buffer4, sizeof(buffer4), 1, ptr);
    header->sample_rate = buffer4[0] |
    						(buffer4[1] << 8) |
    						(buffer4[2] << 16) |
    						(buffer4[3] << 24);

    read = fread(buffer4, sizeof(buffer4), 1, ptr);
    header->byterate  = buffer4[0] |
    						(buffer4[1] << 8) |
    						(buffer4[2] << 16) |
    						(buffer4[3] << 24);

    read = fread(buffer2, sizeof(buffer2), 1, ptr);
    header->block_align = buffer2[0] |
    					(buffer2[1] << 8);

    read = fread(buffer2, sizeof(buffer2), 1, ptr);
    header->bits_per_sample = buffer2[0] | (buffer2[1] << 8);

    read = fread(header->subChunk2Id, sizeof(header->subChunk2Id), 1, ptr);

    read = fread(buffer4, sizeof(buffer4), 1, ptr);
    header->subChunk2Size = buffer4[0] |
    				(buffer4[1] << 8) |
    				(buffer4[2] << 16) |
    				(buffer4[3] << 24 );

    return 0;
}

struct HEADER *makeHeader(const int sampleRate, const short int numChannels,
                            const short int bitsPerSample) {
    struct HEADER *header = malloc(sizeof(struct HEADER));
    header->chunkId[0] = 'R';
    header->chunkId[1] = 'I';
    header->chunkId[2] = 'F';
    header->chunkId[3] = 'F';

    header->format[0] = 'W';
    header->format[1] = 'A';
    header->format[2] = 'V';
    header->format[3] = 'E';

    header->subChunk1Id[0] = 'f';
    header->subChunk1Id[1] = 'm';
    header->subChunk1Id[2] = 't';
    header->subChunk1Id[3] = ' ';

    header->format_type = 1;
    header->channels = numChannels;
    header->sample_rate = sampleRate;
    header->bits_per_sample = bitsPerSample;
    header->byterate = sampleRate * numChannels * bitsPerSample / 8;
    header->block_align = numChannels * bitsPerSample / 8;

    header->subChunk2Id[0] = 'd';
    header->subChunk2Id[1] = 'a';
    header->subChunk2Id[2] = 't';
    header->subChunk2Id[3] = 'a';

    header->chunkSize = 4+8+16+8;
    header->length_of_fmt = 16;
    header->subChunk2Size = 0;

    return header;
}

/* --- end header functions --- */


int main(int argc, char **argv) {

    FILE *ptr;
    struct HEADER header;

    // open file
    printf("Opening file...\n");
    ptr = fopen(argv[1], "rb");
    if (ptr == NULL) {
        int errsv = errno;
        printf("Error opening file. %s\n", strerror(errsv));
        exit(1);
    }

    fillHeader(&header, ptr);

	// skipping over the rest of the header
	long int rmore = header.length_of_fmt - (sizeof(struct HEADER) - 20);
	if ( 0 != fseek(ptr, rmore, SEEK_CUR)) {
		int errsv = errno;
        printf("Error seeking file. %s\n", strerror(errsv));
        exit(1);
	}

    // calculate no.of samples
    //long num_samples = (8 * header.subChunk2Size) / (header.channels * header.bits_per_sample);
    //long size_of_each_sample = (header.channels * header.bits_per_sample) / 8;

    /*
     * Goal: 20s ---| chop 20s |---->
     *
     */
	 /*
	long bytes_in_each_channel = (size_of_each_sample / header.channels);
    if ((bytes_in_each_channel  * header.channels) != size_of_each_sample) {
        printf("Error: %ld x %ud <> %ld\n", bytes_in_each_channel, header.channels, size_of_each_sample);
		return 1;
	}
	*/

	//float secondsTotal = (float) (header.chunkSize / header.byterate)-20;
	//float secondsTotal = (float) (header.chunkSize / header.byterate);
	//unsigned long int totalBytes = num_samples * sizeof(char);

	struct WAV *wave = malloc(sizeof(struct WAV));
	wave->header = makeHeader(header.sample_rate, header.channels, header.bits_per_sample);
	wave->index = 0;
	wave->size = 0;
	wave->numSamples = 0;
	wave->header->subChunk2Size = 0;

	toLittleEndian(sizeof(int), (void*)&(wave->header->chunkSize));
	toLittleEndian(sizeof(int), (void*)&(wave->header->length_of_fmt));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->format_type));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->channels));
	toLittleEndian(sizeof(int), (void *)&(wave->header->sample_rate));
	toLittleEndian(sizeof(int), (void *)&(wave->header->byterate));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->block_align));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->bits_per_sample));
	toLittleEndian(sizeof(int), (void*)&(wave->header->subChunk2Size));

	//wave->data = calloc(1, header.subChunk2Size);
    unsigned int nsamples_20s = (int)(80 * header.sample_rate);
	wave->data = calloc(1, nsamples_20s);
	//size_t read = fread(wave->data, sizeof(char), (int)(header.subChunk2Size/sizeof(char)), ptr);
	size_t read = fread(wave->data, sizeof(char), nsamples_20s, ptr);
	//if( read != 0 || read != (int)(header.subChunk2Size/sizeof(char)) ) {
	if( read != 0 || read != nsamples_20s ) {
		if( ferror(ptr) ) {
			fprintf(stderr, "Error reading %s\n", argv[1]);
			return 1;
		}
	}
	wave->index += read;
	wave->size += sizeof(char)*read;
	wave->header->subChunk2Size += sizeof(char)*read;
	wave->header->chunkSize += sizeof(char)*read;
	wave->numSamples += read;
	fclose(ptr);

	// open the file, write header, write data
	printf("Writing file...\n");
	FILE *file;
	file = fopen("new.wav", "w");
	if ( file == NULL ) {
        int errsv = errno;
        fprintf(stderr, "Error opening file for writing. %s\n", strerror(errsv));
        exit(1);
	}
	read = fwrite(wave->header, sizeof(struct HEADER), 1, file);
	if( read != 1 ) {
		if( ferror(file) ) {
			fprintf(stderr, "Error writing file header.\n");
			exit(1);
		}
	}
	//read = fwrite( (void*)(wave->data), sizeof(char), (int)(header.subChunk2Size/sizeof(char)), file);
	read = fwrite( (void*)(wave->data), sizeof(char), nsamples_20s, file);
	if( read == 0 || read != wave->size ) {
		if( ferror(file) ) {
			fprintf(stderr, "Error writing file data.\n");
			exit(1);
		}
	}
	printf("Done!\n");
	fclose(file);

	free(wave->header);
	free(wave->data);
	free(wave);


    return 0;
}
