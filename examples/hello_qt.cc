#include <QApplication>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QWidget window;
    window.setFixedSize(100,50);

    window.show();
    return app.exec();
}
