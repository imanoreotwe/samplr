#include <unistd.h>
#include <cstdlib>
#include <cerrno>
#include <cstdint>
#include <cstring>

#include <portaudio.h>

#include <string>
#include <iostream>

using namespace std;

struct HEADER {
    char        chunkId[4];         // RIFF String
    int         chunkSize;          // overall size in bytes
    char        format[4];          // WAVE string

    char        subChunk1Id[4];     // fmt string
    int         subChunk1Size;      // length of format subchunk
    short int   audioFormat;        // format type. 1-PCM, 3- IEEE float, 6- 8bit A law, 7- 8bit mu law
    short int   channels;
    int         sampleRate;         // sampling rate (blocks per second)
    int         byteRate;           // SampleRate * Channels * BitsPerSample/8
    short int   blockAlign;         // Channels * BitsPerSample/8
    short int   bitsPerSample;      // bits per sample 8/16

    char        subChunk2Id[4];      // DATA string or FLLR string
    int         subChunk2Size;      // NumSamples * FrameSize
};

struct WAVE {
    struct HEADER *header;
    char* data;
    size_t index;
};

static string program_name;

template<typename T>
T chop_fread(FILE *stream) {
    T value;
    size_t read = fread(&value, sizeof(value), 1, stream);
    if(read != 1 && ferror(stream) ) {
        throw "fread error";
    }
    return value;
}

void chop_fread(FILE *stream, size_t len, char* fill) {
    size_t read = fread(&fill, 1, len, stream);
    if (read != len && ferror(stream) ) {
        throw "fread error";
    }
}

/* --- Endian Functions --- */

int isBigEndian() {
    int test = 1;
    char *p = (char*)&test;

    return p[0] == 0;
}

void reverseEndianness(const long long int size, void* value){
    int i;
    char result[32];
    for( i=0; i<size; i+=1 ){
        result[i] = ((char*)value)[size-i-1];
    }
    for( i=0; i<size; i+=1 ){
        ((char*)value)[i] = result[i];
    }
}

void toBigEndian(const long long int size, void* value){
    char needsFix = !( (1 && isBigEndian()) || (0 && !isBigEndian()) );
    if( needsFix ){
        reverseEndianness(size,value);
    }
}

void toLittleEndian(const long long int size, void* value){
    char needsFix = !( (0 && isBigEndian()) || (1 && !isBigEndian()) );
    if( needsFix ){
        reverseEndianness(size,value);
    }
}

/* --- End Endian --- */

/* --- Header Functions --- */

int fillHeader(struct HEADER *header, FILE *ptr) {
    //unsigned char buffer4[4];
    //unsigned char buffer2[2];

    chop_fread(ptr, 4, header->chunkId);

    // convert little endian to big endian 4 byte int
    header->chunkSize = chop_fread<uint32_t>(ptr);
    /*
    header->chunkSize = buffer4[0] |
    						(buffer4[1]<<8) |
    						(buffer4[2]<<16) |
    						(buffer4[3]<<24);
    */
    chop_fread(ptr, 4, header->format);

    chop_fread(ptr, 4, header->subChunk1Id);

    // convert little endian to big endian 4 byte integer
    header->subChunk1Size = chop_fread<uint32_t>(ptr);
    /*
    header->length_of_fmt = buffer4[0] |
    							(buffer4[1] << 8) |
    							(buffer4[2] << 16) |
    							(buffer4[3] << 24);
    */
    header->audioFormat = chop_fread<uint16_t>(ptr);
    //header->format_type = buffer2[0] | (buffer2[1] << 8);


    header->channels = chop_fread<uint16_t>(ptr);
    //header->channels = buffer2[0] | (buffer2[1] << 8);

    header->sampleRate = chop_fread<uint32_t>(ptr);
    /*
    header->sample_rate = buffer4[0] |
    						(buffer4[1] << 8) |
    						(buffer4[2] << 16) |
    						(buffer4[3] << 24);
    */

    header->byteRate = chop_fread<uint32_t>(ptr);
    /*
    header->byterate  = buffer4[0] |
    						(buffer4[1] << 8) |
    						(buffer4[2] << 16) |
    						(buffer4[3] << 24);
    */

    header->blockAlign = chop_fread<uint16_t>(ptr);
    /*
    header->block_align = buffer2[0] |
    					(buffer2[1] << 8);
    */

    header->bitsPerSample = chop_fread<uint16_t>(ptr);
    //header->bits_per_sample = buffer2[0] | (buffer2[1] << 8);

    chop_fread(ptr, 4, header->subChunk2Id);

    header->subChunk2Size = chop_fread<uint32_t>(ptr);
    /*
    header->subChunk2Size = buffer4[0] |
    				(buffer4[1] << 8) |
    				(buffer4[2] << 16) |
    				(buffer4[3] << 24 );
    */
    return 0;
}

struct HEADER *makeHeader(const int sampleRate, const short int numChannels,
                            const short int bitsPerSample) {
    struct HEADER *header = new HEADER;
    header->chunkId[0] = 'R';
    header->chunkId[1] = 'I';
    header->chunkId[2] = 'F';
    header->chunkId[3] = 'F';

    header->format[0] = 'W';
    header->format[1] = 'A';
    header->format[2] = 'V';
    header->format[3] = 'E';

    header->subChunk1Id[0] = 'f';
    header->subChunk1Id[1] = 'm';
    header->subChunk1Id[2] = 't';
    header->subChunk1Id[3] = ' ';

    header->audioFormat = 1;
    header->channels = numChannels;
    header->sampleRate = sampleRate;
    header->bitsPerSample = bitsPerSample;
    header->byteRate = sampleRate * numChannels * bitsPerSample / 8;
    header->blockAlign = numChannels * bitsPerSample / 8;

    header->subChunk2Id[0] = 'd';
    header->subChunk2Id[1] = 'a';
    header->subChunk2Id[2] = 't';
    header->subChunk2Id[3] = 'a';

    header->chunkSize = 4+8+16+8;
    header->subChunk1Size = 16;
    header->subChunk2Size = 0;

    return header;
}
/* --- end header --- */

/* --- Pulseaudio Functions --- */

static int outputCallback( const void *inputBuffer,
						   void *outputBuffer,
					   	   unsigned long frameCount,
					   	   const PaStreamCallbackTimeInfo* timeInfo,
					   	   PaStreamCallbackFlags statusFlags,
					   	   void *userData )
{
	struct WAVE *wave = (struct WAVE *)userData;
	uint32_t* src = (uint32_t*)wave->data;
	uint8_t *out = (uint8_t*)outputBuffer;
	size_t frameSize = wave->header->channels * (wave->header->bitsPerSample/8);
	size_t maxIndex = wave->header->subChunk2Size / frameSize;

	size_t framesLeft = maxIndex - wave->index;
	long framesEmpty = frameCount - framesLeft;
	src += wave->index;

	if (framesLeft >= frameCount) {
		memcpy(out, src, frameCount * frameSize);
		out = out + frameCount * frameSize;
	} else {
		//put some
		memcpy(out, src, framesLeft * frameSize);
		//zero some
		if (framesEmpty > 0) {
			memset(out+framesLeft * frameSize, 0, framesEmpty * frameSize);
			out = out + frameCount * frameSize;
			wave->index = 0;
			return paComplete;
		}
	}
	wave->index += frameCount;
	return paContinue;
}

void paErrHandler(PaError err, const char *message) {
	Pa_Terminate();
	cerr << program_name << ": " << message << " " << Pa_GetErrorText(err);
}

/* --- End Pulseaudio --- */

void input_loop(PaStream *stream) {
    char c;
    PaError err;
    while(true){
        cin >> c;
        switch(c) {
            case 'p': {
            	// start stream
            	err = Pa_StartStream( stream );
            	if( err != paNoError) {
                    paErrHandler(err, "Portaudio failed to start stream."); exit(1);
                }
                break;
            }
            case 's': {
                // stop stream
            	err = Pa_StopStream( stream ); //Pa_AbortStream()
            	if( err != paNoError) {
                    paErrHandler(err, "Portaudio failed to stop stream."); exit(1);
                }
                break;
            }
            case 'q': {
                return;
            }
        }
    }
}

int main(int argc, char **argv) {
	program_name = argv[0];

    FILE *ptr;
    struct HEADER header;

    // open file
    printf("Opening file...\n");
    ptr = fopen(argv[1], "rb");
    if (ptr == NULL) {
        int errsv = errno;
        cerr << "Error opening file. " << strerror(errsv) << endl;
        exit(1);
    }

    try {
        fillHeader(&header, ptr);
    } catch(string err) {
        cerr << program_name << ": Could not fill header. " << err << endl;
        exit(1);
    }


	// initalizing portaudio
	cout << program_name << ": Initalizing portaudio...\n";
	PaError err = Pa_Initialize();
	if( err != paNoError) {
        paErrHandler(err, "Portaudio failed to initalize.");
        exit(1);
    }

	struct WAVE *wave = new struct WAVE;
	wave->header = makeHeader(header.sampleRate, header.channels, header.bitsPerSample);
	wave->index = 0;
	wave->header->subChunk2Size = 0;

	//wave->data = calloc(1, header.subChunk2Size);
    //unsigned int nsamples_20s = (int)(20 * header.blockAlign * header.sample_rate);
    //unsigned int nsamples_5s = (int)(5 * header.blockAlign * header.sampleRate);
    size_t size = (header.subChunk2Size/sizeof(char));
	wave->data = new char[size];
	size_t read = fread(wave->data, sizeof(char), size, ptr);
	//size_t read = fread(wave->data, sizeof(char), nsamples_5s, ptr);
    if (read != size && ferror(ptr) ) {
        cerr << program_name << ": Error reading samples." << endl;
        exit(1);
    }
	wave->index += read;
	wave->header->subChunk2Size += sizeof(char)*read;
	wave->header->chunkSize += sizeof(char)*read;
	fclose(ptr);

	// opening an io stream
	PaStream *stream;
	wave->index = 0;

	PaStreamParameters outputParams;
	outputParams.device = Pa_GetDefaultOutputDevice();
	if(outputParams.device == paNoDevice) {
		cerr << program_name << "ERROR: No default output device." << endl;
		exit(1);
	}

	outputParams.channelCount = 2;
	outputParams.sampleFormat = paInt16;
	outputParams.suggestedLatency = Pa_GetDeviceInfo( outputParams.device)->defaultLowOutputLatency;
    outputParams.hostApiSpecificStreamInfo = NULL;

	err = Pa_OpenStream(&stream,
		NULL,
		&outputParams,
		header.sampleRate,
		paFramesPerBufferUnspecified,
		0,
		outputCallback,
		wave
	);
	if( err != paNoError) {
        paErrHandler(err, "Portaudio failed to open io stream.");
        exit(1);
    }

    input_loop(stream);

    /*
	//Pa_Sleep(8*1000); // seconds * 1000
	// wait until stream has finished playing
	while(Pa_IsStreamActive(stream) > 0)
		usleep(1000);
    */

	// stream cleanup
	err = Pa_CloseStream( stream );
	if( err != paNoError ) {
        paErrHandler(err, "Portaudio failed to close stream."); exit(1);
    }

	err = Pa_Terminate();
	if( err != paNoError ) {
        paErrHandler(err, "Portaudio filed to terminate."); exit(1);
    }

    /*
	toLittleEndian(sizeof(int), (void*)&(wave->header->chunkSize));
	toLittleEndian(sizeof(int), (void*)&(wave->header->length_of_fmt));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->format_type));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->channels));
	toLittleEndian(sizeof(int), (void *)&(wave->header->sample_rate));
	toLittleEndian(sizeof(int), (void *)&(wave->header->byterate));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->block_align));
	toLittleEndian(sizeof(short int), (void*)&(wave->header->bits_per_sample));
	toLittleEndian(sizeof(int), (void*)&(wave->header->subChunk2Size));
    */

    /*
	// open the file, write header, write data
	printf("Writing file...\n");
	FILE *file;
	file = fopen("new.wav", "w");
	if ( file == NULL ) {
        int errsv = errno;
        cerr << program_name << "Error opening file for writing. " << strerror(errsv) << endl;
        exit(1);
	}
	read = fwrite(wave->header, sizeof(struct HEADER), 1, file);
	if( read != 1 ) {
		if( ferror(file) ) {
			cerr << "Error writing file header." << endl;
			exit(1);
		}
	}
	//read = fwrite( (void*)(wave->data), sizeof(char), (int)(header.subChunk2Size/sizeof(char)), file);
	read = fwrite( (void*)(wave->data), sizeof(char), nsamples_5s, file);
	if( read == 0 || read != wave->header->subChunk2Size ) {
		if( ferror(file) ) {
			cerr << "Error writing file data." << endl;
			exit(1);
		}
	}
    cout << "Done!\n";
	fclose(file);
    */

	// cleanup
	delete wave->header;
	delete[] wave->data;
	delete wave;


    return 0;
}
